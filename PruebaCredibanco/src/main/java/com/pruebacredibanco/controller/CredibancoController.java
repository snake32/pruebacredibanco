package com.pruebacredibanco.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pruebacredibanco.dto.Usuario;
import com.pruebacredibanco.service.ICredibancoService;

@RestController
@RequestMapping("/Credibanco")
public class CredibancoController {
	
	@Autowired
	private ICredibancoService credibancoService;
	
	@PostMapping("/autenticacion")
	public void autenticacion(@RequestBody Usuario objUsuario) {
		credibancoService.autenticar(objUsuario);
	}

}
