package com.pruebacredibanco.config;

import org.springframework.ws.client.core.support.WebServiceGatewaySupport;

public class SOAPConexion extends WebServiceGatewaySupport {
	
	public Object llamadoService(String url, Object request) {
		return getWebServiceTemplate().marshalSendAndReceive(url, request);
	}

}
