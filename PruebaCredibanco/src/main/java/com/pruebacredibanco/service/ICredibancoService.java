package com.pruebacredibanco.service;

import com.pruebacredibanco.dto.Usuario;

public interface ICredibancoService {
	
	public void autenticar(Usuario objUsuario);

}
